import pandas as pd
import xlsxwriter
import requests
from bs4 import BeautifulSoup

def getWebList():
    allPagesLink = [
        'http://www.sobsuan.com/modules.php?name=Forums&file=viewforum&f=17',
        'http://www.sobsuan.com/modules.php?name=Forums&file=viewforum&f=64',
        'http://www.sobsuan.com/modules.php?name=Forums&file=viewforum&f=22',
        'http://www.sobsuan.com/modules.php?name=Forums&file=viewforum&f=40',
        'http://www.sobsuan.com/modules.php?name=Forums&file=viewforum&f=20',
        'http://www.sobsuan.com/modules.php?name=Forums&file=viewforum&f=53'
    ]
    allTopicLink = []
    AllTopicInLink = []
    all_postbody = []
    # Count the pages and get all topic links
    for i in range(len(allPagesLink)):
        print('Searching in >> ' + allPagesLink[i])
        pagesNum = getPages(allPagesLink[i])
        print('This URL has %s pages' %pagesNum)
        for page in range(pagesNum):
            startNum = page * 25
            start = str(startNum)
            AllTopicInPage = getTopics(allPagesLink[i] + '&start=' + str(start))
            for topic in range(len(AllTopicInPage)):
                allTopicLink.append(AllTopicInPage[topic])
                AllTopicInLink.append(AllTopicInPage[topic])
        webList = pd.DataFrame({'WebList': allTopicLink})
        writer1 = pd.ExcelWriter('WebList.xlsx', engine='xlsxwriter')
        webList.to_excel(writer1, sheet_name='WebList')
        writer1.save()
        for topic in range(len(AllTopicInLink)):
            PostInTopic = getDataInTopic(AllTopicInLink[topic])
            for post in range(len(PostInTopic)):
                all_postbody.append(PostInTopic[post])
                print('postbody is appending...')
            TopicPost = pd.DataFrame({'postbody': all_postbody})
            writer2 = pd.ExcelWriter('data_'+str(i+1)+'.xlsx', engine='xlsxwriter')
            TopicPost.to_excel(writer2, sheet_name='data', index=False)
            writer2.save()
        AllTopicInLink = []

def getPages(url):
    count_topic = 0
    start = 0
    check = 0
    while (check == 0):
        link = url + '&start=%s' %(start*25)
        timeout = 5
        r = requests.get(link, timeout).content
        soup = BeautifulSoup(r, "html.parser")
        linkList = soup.find_all('a')
        for l in linkList:
            linkInList = l.get('href')
            if (linkInList is not None):
                substr = linkInList[0:41]
                if (substr == 'modules.php?name=Forums&file=viewtopic&t='):
                    # print('linkInList = %s'%linkInList)
                    # print('pass!')
                    count_topic += 1
                    break
        if(count_topic == 0):
            return start
        # print('start %s is finish!'%(start*25))
        start += 1
        count_topic = 0
    return start

def getTopics(url):
    print('Finding topic in >>> ' + url)
    link = []
    count_topic = 0
    timeout = 5
    r = requests.get(url, timeout).content
    soup = BeautifulSoup(r, "html.parser")
    linkList = soup.find_all('a')
    for l in linkList:
        linkInList = l.get('href')
        if (linkInList is not None):
            substr = linkInList[0:41]
            if (substr == 'modules.php?name=Forums&file=viewtopic&t='):
                # print("['{0}'] => '{1}'".format(count_topic, linkInList))
                # print('pass!')
                link.append('http://www.sobsuan.com/' + linkInList)
                count_topic += 1
    return link

def getDataInTopic(url):
    print('Scraping >> '+url)
    start_str = url.split('&')
    start_split = start_str[0:6]
    timeout = 5
    r = requests.get(url, timeout).content
    soup = BeautifulSoup(r, "html.parser")
    postbody = soup.find_all('span', class_='postbody')
    post_data = []
    post_count = 0
    for post in range(len(postbody)):
        if (post_count == 0 and start_split != 'start='):
            post_data.append('[SUBJECT]'+postbody[post].text)
        else:
            post_data.append(postbody[post].text)
        post_count += 1
    return post_data

getWebList()